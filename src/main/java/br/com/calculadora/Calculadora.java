package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Calculadora {

    public Calculadora() {
    }

    public String nome;

    public int soma(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero +  segundoNumero;
        return resultado;
    }

    public double soma(double primeiroNumero, double segundoNumero){
        Double resultado = primeiroNumero + segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);

        return bigDecimal.doubleValue();
    }

    public long multiplica(int primeiroNumero, int segundoNumero) {
        long resultado = primeiroNumero * segundoNumero;
        return  resultado;
    }

    public double multiplica(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero *  segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(4, RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();
    }

    public double divisao(int primeiroNumero, int segundoNumero) {
        double resultado = 0;
        if(primeiroNumero > segundoNumero){
            resultado = primeiroNumero / segundoNumero;
        }
        else
        {
            resultado = segundoNumero /primeiroNumero;
        }
        return resultado;
    }

    public double divisao(double primeiroNumero, double segundoNumero) {
        double resultado = 0;
        if(primeiroNumero > segundoNumero){
            resultado = primeiroNumero / segundoNumero;
        }
        else
        {
            resultado = segundoNumero /primeiroNumero;
        }
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(2, RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();
    }
}
