package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp(){
        calculadora = new Calculadora();
        calculadora.nome = "Carol";
    }

    @Test
    public void testaASomaDeDoisNumerosInteiros() {
        int resultado = calculadora.soma(1, 2);
        calculadora.nome = "Carlos";
        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaASomaDeDoisNumerosNaoNaturais(){
        double resultado = calculadora.soma(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaMultiplicacaodeDoisNumerosInteiros(){
        long resultado = calculadora.multiplica(2 , 4);

        Assertions.assertEquals(8, resultado);
    }

    @Test
    public void testaMultiplicacaodeDoisNumerosInteirosNegativos(){
        long resultado = calculadora.multiplica(-2 , 4);

        Assertions.assertEquals(-8, resultado);
    }

    @Test
    public void testaMultiplicacaodeDoisNumerosFlutuantes(){
        double resultado = calculadora.multiplica(2.2,  4.2);

        Assertions.assertEquals(9.24, resultado);
    }

    @Test
    public void testaDivisaoDeDoisNumerosInteiros(){
        double resultado = calculadora.divisao(5 , 2);

        Assertions.assertEquals(2.5, resultado);
    }

    @Test
    public void testaDivisaoDeDoisNumerosInteirosNegativos(){
        double resultado = calculadora.divisao(4 , -2);

        Assertions.assertEquals(-2, resultado);
    }

    @Test
    public void testaDivisaoDeDoisNumerosFlutuantes(){
        double resultado = calculadora.divisao(4.2 , 2.2);

        Assertions.assertEquals(1.91, resultado);
    }
}
